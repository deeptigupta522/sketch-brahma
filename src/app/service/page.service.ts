import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { dashboard } from '../model/dashboard.model';

@Injectable({
  providedIn: 'root'
})
export class PageService {

  public api_url:string;
  constructor(private http: HttpClient ) {
    this.api_url = `${environment.api}`
   }

   getPageData(){
     return this.http.get<dashboard[]>(this.api_url+'?by_state=california&per_page=50');
   }
}
