import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PerPageCountryComponent } from './per-page-country/per-page-country.component';

const routes: Routes = [
  { path: 'Dashboard', component:DashboardComponent },
  {
    path:'PerPage',
    component:PerPageCountryComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
