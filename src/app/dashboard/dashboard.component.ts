import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';
import { dashboard } from '../model/dashboard.model';
import { DashboardService } from '../service/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  private destroy$ = new Subject();


  TableList: dashboard[]=[];

  constructor(private TableService : DashboardService) { }

  ngOnInit(): void {
    this.cardData();
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  
  cardData(){
    this.TableService.getData().pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.TableList =data;
    })
  }

}
