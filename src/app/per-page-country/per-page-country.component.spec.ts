import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerPageCountryComponent } from './per-page-country.component';

describe('PerPageCountryComponent', () => {
  let component: PerPageCountryComponent;
  let fixture: ComponentFixture<PerPageCountryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerPageCountryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerPageCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
