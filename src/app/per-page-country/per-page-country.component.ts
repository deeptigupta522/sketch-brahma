import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { dashboard } from '../model/dashboard.model';
import { PageService } from '../service/page.service';

@Component({
  selector: 'app-per-page-country',
  templateUrl: './per-page-country.component.html',
  styleUrls: ['./per-page-country.component.scss']
})
export class PerPageCountryComponent implements OnInit {

  private destroy$ = new Subject();


  List: dashboard[]=[];

  constructor(private PageService : PageService) { }

  ngOnInit(): void {
    this.cardData();
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  
  cardData(){
    this.PageService.getPageData().pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.List =data;
    })
  }

}
